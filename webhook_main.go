package main

import (
	"fmt"

	"gopkg.in/go-playground/webhooks.v5/bitbucket-server"
	"net/http"
)

const (
	path = "/webhooks"
)

func main() {
	hook, _ := bitbucketserver.New()

	http.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {

		payload, err := hook.Parse(r, bitbucketserver.PullRequestOpenedEvent, bitbucketserver.PullRequestDeclinedEvent)
		if err != nil {
			fmt.Fprintln(w, "Deactivating Listener....\n  Error Found: ", err)
		}
		if err == bitbucketserver.ErrEventNotFound {
			fmt.Fprintln(w, "Event Not Found!\n")
		} else if err == bitbucketserver.ErrHMACVerificationFailed {
			fmt.Fprintln(w, "Secret is incorrect")
		} else {
			fmt.Fprint(w, err)
		}

		if err == nil {
			fmt.Fprint(w, "Event Found Activating listener")
			switch payload.(type) {
			case bitbucketserver.PullRequestOpenedPayload:
				pullRequest := payload.(bitbucketserver.PullRequestOpenedPayload)
				fmt.Printf("Pull Request Opened:", pullRequest.PullRequest.FromRef.Repository.Project.Name)
			}
		}

	})

}
